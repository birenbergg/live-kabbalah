<?php

Route::resource('courses', 'CoursesController');
Route::resource('user', 'UserController');

Route::get('/', 'HomeController@index')->name('home');

// Route::get('/what-is-kabbalah', 'WhatIsKabbalahController@show');
// Route::get('/what-is-kabbalah/{sub}', 'WhatIsKabbalahController@show');
// Route::get('/what-is-kabbalah/{sub}/{article}', 'WhatIsKabbalahController@show');

// Route::get('/to-live-kabbalah', 'ToLiveKabbalahController@show');
// Route::get('/to-live-kabbalah/{sub}', 'ToLiveKabbalahController@show');
// Route::get('/to-live-kabbalah/{sub}/{article}', 'ToLiveKabbalahController@show');

Route::get('/community', 'CommunityController@show');
Route::get('/community/{article}', 'CommunityController@show');

Route::get('/donations', 'SharedController@donations');
Route::get('/about', 'SharedController@about');
Route::get('/contact', 'SharedController@contact');


Route::get('/events', 'EventsController@show')->name('events');

Route::get('/hebcaltest', function() {
	return view('hebcaltest');
})->name('hebcaltest');



Route::get('/courses/add/{id}', 'CoursesController@addCourse');

Route::get('/courses/{course}/{lesson}', 'LessonsController@show');
Route::get('/courses/{course}/{lesson}/edit', 'LessonsController@edit');
Route::put('/lessons-update/{lesson}', 'LessonsController@update')->name('lessons.update');


Route::get('/setlocale/{lang}', 'LocaleController@setLocale')->name('setlocale');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('user.dashboard');
Route::post('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');

Route::get('/admin', 'AdminController@index')->name('admin.dashboard');

Route::get('/{page}', 'PageController@show');
Route::get('/{page}/{tab}', 'PageController@show');
Route::get('/{page}/{tab}/{link}', 'PageController@show');
Route::get('/{page}/{tab}/{link}/{sublink}', 'PageController@show');