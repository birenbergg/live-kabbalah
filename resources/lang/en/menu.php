<?php

return [
    'home_link'                 => 'Home',
    'intro_link'               => 'What Is Kabbalah?',
    'live_link'               => 'Live Kabbalah',
    'university_link'           => 'University',
    'community_link'            => 'Community',
    'events_link'               => 'Events',
    'site_management_link'      => 'Site Management',

    'register_link'             => 'Register',
    'login_link'                => 'Login',

    'dashboard_link'            => 'Dashboard',
    'logout_link'               => 'Logout',
];