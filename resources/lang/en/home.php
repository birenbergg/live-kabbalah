<?php

return [

	'welcome_title' => 'Wecome to Live Kabbalah<br />Winner’s Consciousness with Rabbi Shaul Youdkevitch',

	'welcome_p1' => 'The monthly, weekly and daily Kabbalah Workout Study is a requirement, it is necessary to develop a consciousness that can overcome challenges and turn them into opportunities.',

	'welcome_p2' => 'Understanding The Kabbalistic Calendar is a very important tool in order to control the dimension of time as part of Winner’s Consciousness – this is Kabbalah’s Consciousness, the consciousness of the Tree of Life.',

	'audio_version_button' => 'Listen to Audio Version',
	'podcast_button' => 'Weekly Zohar Podcast',

	'monthly_study' => 'Monthly Study:',
	'read_article' => 'Read an article about',
	'listen_to_lesson' => 'Listen to Lesson about',

	'this_month_sequences' => 'This Month\'s Sequences',

	'perfect_world_museum_title' => 'Perfect World Museum',

	'perfect_world_museum_p1' => 'Perfect World Museum has been established in order to serve humanity by being a place to present the ideas and the art of those people along the generations who’d used, and are still using, Kabbalah as their language in their quest to achieve a better, more perfect world to all mankind.',
	'perfect_world_museum_p2' => 'Hebrew lettering and/or Kabbalistic symbols on each item, including the holy names of God and angels, promise to bring spiritual forces of good fortune, health and protection to its owner.',
	'perfect_world_museum_p3' => '',
	'go_to_museum_button' => 'Go to Museum',

	'museum_product_1' => 'Challah Cover with Birkat Kohanim Embroidery and 72 names',
	// 'museum_product_2' => 'Brit Mila (Circumcision) Cushion with Kabbalistic Names of Protection Embroidery',
	'museum_product_2' => 'Electroforming Silver Letters Pendant - Pei Hei Lamed פ.ה.ל',
	'museum_product_3' => 'Home Protection Wall Tapestry Kabbalah Holy Names',
	'museum_product_4' => 'Tallit & Tefillin Bags - 72 Names Embroidery',

	'donations_p1' => '',
	'donations_p2' => '',
	'donations_p3' => 'Join our efforts to change the world, educate people, maintain and develop this site.',
	'donations_p4' => 'To support our activities by donating to Live Kabbalah',
	'donations_click' => 'click here',
];