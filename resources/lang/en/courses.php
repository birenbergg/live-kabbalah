<?php

return [

	'preface_p1' => 'The mission of the Live Kabbalah University is to empower individuals and communities to create a world of peace, love, joy, and harmony for humanity.',
	'preface_p2' => 'Live Kabbalah University will inspire people to take action, and provide them with the tools to create thriving communities around the world. These communities will provide a safe, loving and empowering environment for every human being, based on the teachings of the great prophets of the Bible as well as on the teachings of the Kabbalistic Sages that followed their courageous vision.',
	'preface_p3' => 'The university website was designed to support communities and individuals by providing services and supplying educational materials on different levels of Kabbalah study.',
	'preface_p4' => 'The educational material published in the site is varied and renewed frequently. Among other things published you could find lessons designed for beginners, and advanced students.',

	
	'lk_title'					=> 'Live Kabbalah University',
    'level'             		=> 'Level',
    'lessons_num'      		=> 'Number of Lessons',

    'lesson'					=> 'Lesson',
    'you_added'					=> 'You\'ve added this course to your courses',

    'my_courses'				=> 'My Courses',

    'take_this_course'			=> 'Take this course',

    'edit_course'               => 'Edit Course',
    'edit_lesson'              => 'Edit Lesosn',
    'edit_profile'              => 'Edit profile',

    'beginner'                  => 'Beginner',
    'intermediate'              => 'Intermediate',
    'advanced'                  => 'Advanced',

    'price'                     => 'Price',

    'edit'                     => 'Edit',

];