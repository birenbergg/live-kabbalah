<?php

return [

    'username_label'                    => 'Username',
    'password_label'                    => 'Password',
    'remember_me_label'                 => 'Remember me',
    'name_label'                        => 'Name',
    'email_label'                       => 'Email',
    'confirm_password_label'            => 'Confirm Password',
    'gender_label'                      => 'Gender',
    'birth_year_label'                  => 'Birth Year',
    'birth_month_label'                 => 'Birth Month',
    'birth_date_label'                  => 'Birth Day',
    'name_in_english_label'             => 'Name (in English)',
    'name_in_hebrew_label'              => 'Name (in Hebrew)',
    'description_in_english_label'      => 'Description (in English)',
    'description_in_hebrew_label'       => 'Description (in Hebrew)',

    'code_name_label'                   => 'Code Name',
    'level_label'                       => 'Level',
    'price_usd_label'                   => 'Price (in USD)',
    'price_ils_label'                   => 'Price (in ILS)',
    'category_label'                    => 'Category',
    'is_required_label'                 => 'Is Required?',

    'login_button'                      => 'Login',
    'register_button'                   => 'Register',
    'save_button'                       => 'Save',

    'forgot_password_link'              => 'Forgot password?',
    'login_header'                      => 'Login',
    // 'admin_login_header'                => 'Admin Login',
    'register_header'                   => 'Register',

    'not_set'                           => '- Not set -',

    'male'                              => 'Male',
    'female'                            => 'Female',

    'yes'                               => 'Yes',
    'no'                                => 'No',

    'status_en'                         => 'Status of English Version',
    'status_he'                         => 'Status of Hebrew Version',

    'hidden'                            => 'Hidden',
    'coming_soon'                       => 'Coming Soon',
    'published'                         => 'Published',
];
