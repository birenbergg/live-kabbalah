<?php

return [

	'live_tab' => 'Live Kabbalah',
		'preface_tab' => 'Live Kabbalah',
		'success_tab' => 'Success & Kabbalah',
		// 'love_tab' => '',
		// 'healing_tab' => '',
		'happiness_tab' => 'The Secret for Happiness & Kabbalah',

	'weekly_portions_tab' => 'Weekly Portions',

	'time_gates_tab' => 'Time Gates',

	'life_circles_tab' => 'Life Circles',
];