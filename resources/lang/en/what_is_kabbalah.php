<?php

return [

	'whatis_tab' => 'Kabbalah for Beginners',
		// '' => '',


	'basic_concepts_tab' => 'Basic Concepts in Kabbalah',
		// 'light_tab' => 'Light (Or) and The Creation',
		// 'meditation_tab' => 'Kavana - Meditation according to Kabbalah',
		// 'dreams_tab' => '',
		// 'life_rules_tab' => '',
		// 'branch_language_tab' => '',
		// 'ana_bekoah_tab' => 'The Prayer: Ana BeKho\'ah - The Name of 42 Letters',
		// 'thirteen_attributes_tab' => 'The Thirteen Attributes',

	'kabbalists_tab' => 'Kabbalists',
		// 'rashbi_tab' => 'Rabbi Shimon Bar Yohai',
		// 'ari_tab' => 'The Ari - Rabbi Isaac Luria',
		// 'ashlag_tab' => '',

	'sources_tab' => 'Sources',
		// 'yetzira_tab' => 'The Book of Formation - Sefer Yetsira',
		// 'zohar_tab' => 'The Zohar',
];