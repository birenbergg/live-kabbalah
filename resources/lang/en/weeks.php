<?php

return [
	'portion'  => 'Weekly Portion',

    'this_week' => 'This Week',
    'prev_week' => 'Previous Week',

    'reading'                   => 'Reading',
    'video'                     => 'Video',
    'audio'                     => 'Audio',
    'fb_video'               => 'Facebook Video',
    
];