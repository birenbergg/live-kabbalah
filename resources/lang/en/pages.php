<?php

return [

	'home_title' => 'Live Kabbalah',

	'intro_title' => 'What Is Kabbalah?',
	'intro_header' => 'What Is Kabbalah?',
	
	'live_kabbalah_title' => 'Live Kabbalah',
	'live_kabbalah_header' => 'Live Kabbalah',

	'courses_title' => 'Live Kabbalah University',
	'courses_header' => 'Live Kabbalah University',

	'community_title' => 'Community',
	'community_header' => 'Community',

	'events_title' => 'Events',
	'events_header' => 'Events',

	'donations_title' => 'Donations',
	'donations_header' => 'Donations',

	'about_title' => 'About',
	'about_header' => 'About',

	'contact_title' => 'Conatct Us',
	'contact_header' => 'Contact Us',
];