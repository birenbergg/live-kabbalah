<?php

return [

	'about_tab' => 'About the Community',
	'vision_tab' => 'Our Vision',
	'rules_tab' => 'Rules and Regulations',
	'beehive_tab' => 'What is a Beehive?',
	'beehive_rules_tab' => 'Ground rules for “Beehive” Groups',

];