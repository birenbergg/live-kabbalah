<?php

return [

	'whatis_tab' => 'קבלה למתחיל',
		// '' => '',

	'basic_concepts_tab' => 'מושגים בסיסיים בקבלה',
		// 'light_tab' => 'אור',
		// 'meditation_tab' => 'כוונה – מדיטציה עפ״י הקבלה',
		// 'dreams_tab' => 'חלומות',
		// 'life_rules_tab' => 'חוקי החיים',
		// 'branch_language_tab' => 'שפת הענפים',
		// 'ana_bekoah_tab' => 'תפילת אנא בכח – שם בן מ״ב',
		// 'thirteen_attributes_tab' => 'י״ג מידות',

	'kabbalists_tab' => 'מקובלים',
		// 'rashbi_tab' => 'ר\' שמעון בר יוחאי',
		// 'ari_tab' => 'האר"י',
		// 'ashlag_tab' => 'ר\' יהודה לייב הלוי אשלג',

	'sources_tab' => 'מקורות',
		// 'yetzira_tab' => 'ספר היצירה',
		// 'zohar_tab' => 'ספר הזוהר',
	
];