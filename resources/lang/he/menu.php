<?php

return [
    'home_link'                 => 'דף הבית',
    'intro_link'               => 'מהי קבלה?',
    'live_link'               => 'לחיות קבלה',
    'university_link'           => 'אוניברסיטה',
    'community_link'            => 'קהילה',
    'events_link'               => 'אירועים',
    'site_management_link'      => 'ניהול אתר',

    'register_link'             => 'הרשם',
    'login_link'                => 'הכנס',

    'dashboard_link'            => 'דשבורד',
    'logout_link'               => 'צא',
];