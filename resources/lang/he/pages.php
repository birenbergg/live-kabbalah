<?php

return [

	'home_title' => 'חיים קבלה',
	
	'intro_title' => 'מהי קבלה?',
	'intro_header' => 'מהי קבלה?',
	
	'live_kabbalah_title' => 'לחיות קבלה',
	'live_kabbalah_header' => 'לחיות קבלה',

	'courses_title' => 'האוניברסיטה של חיים קבלה',
	'courses_header' => 'האוניברסיטה של חיים קבלה',

	'community_title' => 'קהילה',
	'community_header' => 'קהילה',
	
	'events_title' => 'אירועים',
	'events_header' => 'אירועים',

	'donations_title' => 'תרומות',
	'donations_header' => 'תרומות',

	'about_title' => 'אודות',
	'about_header' => 'אודות',

	'contact_title' => 'יצירת קשר',
	'contact_header' => 'יצירת קשר',
];