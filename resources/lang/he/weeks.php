<?php

return [
	'portion'	=> 'פרשת השבוע בזוהר',

	'this_week'	=> 'שבוע הזה',
	'prev_week'	=> 'שבוע שעבר',

	'reading'                   => 'קריאה',
    'video'                     => 'וידאו',
    'audio'                     => 'אודיו',
    'fb_video'               => 'וידאו בפייסקוב',
    
];