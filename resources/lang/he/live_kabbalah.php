<?php

return [

	'live_tab' => 'לחיות קבלה',
		'preface_tab' => 'לחיות קבלה',
		'success_tab' => 'סוד ההצלחה עפ״י הבקלה',
		'love_tab' => 'מהי אהבה עפ״י הבקלה',
		'healing_tab' => 'סודות הריפוי עפ״י הקבלה',
		'happiness_tab' => 'להיות מאושרים',

	'weekly_portions_tab' => 'פרשת השבוע',

	'time_gates_tab' => 'שערים בזמן',

	'life_circles_tab' => 'מעגלי חיים',
	
];