<?php

return [

    'username_label'			        => 'שם משתמש',
    'password_label'			        => 'סיסמה',
    'remember_me_label'			        => 'זכור אותי',
    'name_label'				        => 'שם',
    'email_label'				        => 'דוא״ל',
    'confirm_password_label'	        => 'אימות סיסמה',
    'gender_label'                      => 'מין',
    'birth_year_label'                  => 'שנת לידה',
    'birth_month_label'                 => 'חודש לידה',
    'birth_date_label'                  => 'יום לידה',
    'name_in_english_label'             => 'שם (באנגלית)',
    'name_in_hebrew_label'              => 'שם (בעברית)',
    'description_in_english_label'      => 'תאור (באנגלית)',
    'description_in_hebrew_label'       => 'תאור (בעברית)',

    'code_name_label'                   => 'שם קוד',
    'level_label'                       => 'רמה',
    'price_ils_label'                   => 'מחיר (בשקלים)',
    'price_usd_label'                   => 'מחיר (בדולר ארה״ב)',
    'category_label'                    => 'קטגוריה',
    'is_required_label'                 => 'האם נחוץ?',
    
    'login_button'                      => 'הכנס',
    'register_button'                   => 'הרשם',
    'save_button'                       => 'שמור',

    'forgot_password_link'              => 'שכחתי סיסמה',
    'login_header'                      => 'כניסה',
    // 'admin_login_header'                => 'כניסת מנהל',
    'register_header'                   => 'הרשמה',

    'not_set'                           => '- לא הוגדר -',

    'male'                              => 'זכר',
    'female'                            => 'נקבה',

    'yes'                               => 'כן',
    'no'                                => 'לא',

    'status_en'                         => 'סטטוס של גרסה אנגלית',
    'status_he'                         => 'סטטוס של גרסה עברית',

    'hidden'                            => 'מוסתר',
    'coming_soon'                       => 'בקרוב',
    'published'                         => 'מפורסם',
];
