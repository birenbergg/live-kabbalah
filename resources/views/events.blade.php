@extends('layout.master')

@section('title')
    {!! trans("pages.events_title") !!}
@stop

@section('content')

	<div style="width:100%; padding: 0 0 8em;">

        <div>
            <h1 style="text-align: center; font-size: 56px; display: flex; flex-direction: column; align-items: center;">
                {!! $current_page['title_'.config('app.locale')] !!}
            </h1>     
        </div>

        <div class="page-content">

			<h2>אירועים בירושלים</h2>
			@foreach($j_events as $event)
				<div>
					<h3>{{ $event['title_'.config('app.locale')] }}</h3>
					<date>{{ $event->date }}</date>
				</div>
			@endforeach

			<h2>אירועים בהרצליה</h2>
			@foreach($h_events as $event)
				<div>
					<h3>{{ $event['title_'.config('app.locale')] }}</h3>
				</div>
			@endforeach
        </div>
    </div>
    
@endsection