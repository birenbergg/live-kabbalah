@extends('layout.master')

@section('title')
	עריכה: {{ $user->name }}
@endsection

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::model($user, array('route' => array('user.update', $user->username), 'method' => 'PUT')) }}
		{{ Form::open(array('url' => 'user')) }}
			
			<div class="edit-block">
				<div>
					{{ Form::label('name', trans('forms.name_label')) }}
					{{ Form::text('name', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('email', trans('forms.email_label')) }}
					{{ Form::text('email', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('username', trans('forms.username_label')) }}
					{{ Form::text('username', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('gender_id', trans('forms.gender_label')) }}
					{{ Form::select('gender_id', array(null => trans('forms.not_set'), '1' => trans('forms.male'), '2' => trans('forms.female'),), null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					@php
						$years = [];
						
						$years[null] = trans('forms.not_set');

						for($i = date('Y') - 120; $i < date('Y') + 1; $i++) {
							$years[$i] = $i;
						}

					@endphp

					{{ Form::label('birth_year', trans('forms.birth_year_label')) }}
					{{ Form::select('birth_year', $years, null) }}
				</div>
				<div>
					@php
						$months = [
							null => trans('forms.not_set'),
							'1' => trans('months.jan'),
							'2' => trans('months.feb'),
							'3' => trans('months.mar'),
							'4' => trans('months.apr'),
							'5' => trans('months.may'),
							'6' => trans('months.jun'),
							'7' => trans('months.jul'),
							'8' => trans('months.aug'),
							'9' => trans('months.sep'),
							'10' => trans('months.oct'),
							'11' => trans('months.nov'),
							'12' => trans('months.dec')
						]
					@endphp

					{{ Form::label('birth_month', trans('forms.birth_month_label')) }}
					{{ Form::select('birth_month', $months, null) }}
				</div>
				<div>
					@php
						$days = [];
						
						$days[null] = trans('forms.not_set');

						for($i = 1; $i <= 31; $i++) {
							$days[$i] = $i;
						}

					@endphp

					{{ Form::label('birth_date', trans('forms.birth_date_label')) }}
					{{ Form::select('birth_date', $days, null) }}
				</div>
			</div>

			{{ Form::submit(trans('forms.save_button'), array('class' => 'save')) }}
		{{ Form::close() }}
	</div>
@endsection