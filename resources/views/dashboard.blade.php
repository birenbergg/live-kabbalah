@extends('layout.sup_master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div>
                    <h3>{{ trans('home.my_courses') }}</h3>
                    @foreach(Auth::user()->courses as $course)
                        <div>
                            <a href="/courses/{{ $course->slug }}">
                                {{ $course['name_' . config('app.locale')] }}
                            </a>
                        </div>
                    @endforeach
                </div>
                <div><a href="/user/{{ Auth::user()->username }}/edit">{{ trans('home.edit_profile') }}</a></div>
            </div>
        </div>
    </div>
</div>
@endsection