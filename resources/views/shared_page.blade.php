@extends('layout.master')

@section('title')
    {!! $current_page['title_'.config('app.locale')] !!}
    {{ $article['title_'.config('app.locale')] }}
@stop

@section('content')

    <div style="width:100%; padding: 0 0 8em;">

        <div>
            <h1 style="text-align: center; font-size: 56px; display: flex; flex-direction: column; align-items: center;">
            	{{ $article['title_'.config('app.locale')] }}
            </h1>     
        </div>

        <div class="page-content">
			{!! $article['text_'.config('app.locale')] !!}
        </div>
    </div>
@endsection