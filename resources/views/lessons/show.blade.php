@extends('layout.master')

@section('title')
    {{ trans('home.lesson') }} {{ $lesson->index_number }}:
    {{ $lesson['name_' . config('app.locale')] }}
@endsection

@section('content')
    <div>
        <div>
            <h2>
                {{ trans('home.lesson') }} {{ $lesson->index_number }}
            </h2>
            <h3>
                {{ $lesson['name_' . config('app.locale')] }}
            </h3>
            <div>
                {{ $lesson['description_' . config('app.locale')] }}
            </div>
        </div>

        <div><br /></div>

        @if (Auth::user() && Auth::user()->role == 0)
            <form style="margin-bottom:2em;">
                <a class="save" style="display: block; text-align: center; text-decoration: none;" href="/courses/{{ $lesson->course->slug }}/{{ $lesson->index_number }}/edit">{{ trans('home.edit_session') }}</a>
            </form>
        @endif

        <!-- <div class="lesson"></div> -->
        <!-- <div dir="ltr" style="text-align:center;">
            <button onclick="wavesurfer.skip(-10)">
                -10
            </button>
            <button onclick="wavesurfer.playPause()">
                Play
            </button>
            <button onclick="wavesurfer.skip(10)">
                +10
            </button>
        </div> -->
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    var wavesurfer = WaveSurfer.create({
        container: '.lesson',
        waveColor: '#666',
        progressColor: '#333',
        barWidth: 2
    });

    wavesurfer.load("/audio/{{ config('app.locale') }}/courses/{{ $lesson->course->slug }}/{{ sprintf('%03d', $lesson->index_number) }}.mp3");
</script>
@endsection