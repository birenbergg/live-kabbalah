@extends('layout.master')

@section('title')
	{{ trans('home.edit') }}: {{ $lesson['name_' . config('app.locale')] }}
@endsection

@section('content')
	<div class="container">
		{{ HTML::ul($errors->all()) }}

		{{ Form::model($lesson, array('route' => array('sessions.update', $lesson->id), 'method' => 'PUT')) }}
		{{ Form::open(array('url' => 'sessions')) }}
			
			<div class="edit-block">
				<div>
					{{ Form::label('name_he', trans('forms.name_in_hebrew_label')) }}
					{{ Form::text('name_he', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('name_en', trans('forms.name_in_english_label')) }}
					{{ Form::text('name_en', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('description_he', trans('forms.description_in_hebrew_label')) }}
					{{ Form::textarea('description_he', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('description_en', trans('forms.description_in_english_label')) }}
					{{ Form::textarea('description_en', null) }}
				</div>
			</div>

			{{ Form::submit(trans('forms.save_button'), array('class' => 'save')) }}
		{{ Form::close() }}
	</div>
@endsection