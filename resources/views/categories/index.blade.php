@extends('layout.master')

@section('title', trans('home.lk_title'))

@section('content')
    <style type="text/css">
    .overlay {
        opacity:0.85;
        filter: alpha(opacity = 0.85);
        position:absolute;
        top:0; bottom:0; left:0; right:0;
        display:block;
        z-index:2;
        background:white;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 24px;
        font-weight: bold;
        color: #999;
    }
    </style>
    <div style="width:100%;">
        @foreach($categories as $category)
            <h2>{{ $category['name_' . config('app.locale')] }}</h2>
                <div class="course-block">
                    @foreach($category->courses as $course)
                        @if ($course['status_' . config('app.locale')] !== 0 || Auth::guard('admin')->check())
                            @if ($course['status_' . config('app.locale')] !== 1 || Auth::guard('admin')->check())<a href="/courses/{{ $course->slug }}" style="color: black; text-decoration: none;">@endif
                                <div class="course-thumbnail">
                                    <div class="image-wrapper"></div>
                                    <div class="course-thumbnail-text">
                                        <h4>{{ $course['name_' . config('app.locale')] }}</h4>
                                        <div>
                                            <div>{{ trans('home.level') }}: {{ $course->level['name_' . config('app.locale')] }}</div>
                                            <div>{{ trans('home.sessions_num') }}: {{ count($course->sessions) }}</div>
                                        </div>
                                    </div>
                                    @if ($course['status_' . config('app.locale')] == 1)
                                    <div class="overlay">{{ trans('forms.coming_soon') }}</div>
                                    @endif

                                    @if ($course['status_' . config('app.locale')] === 0 && Auth::guard('admin')->check())
                                    <div class="overlay">{{ trans('forms.hidden') }}</div>
                                    @endif
                                </div>
                            @if ($course['status_' . config('app.locale')] !== 1 || Auth::guard('admin')->check())</a>@endif
                        @endif
                    @endforeach
                </div>
        @endforeach
    </div>
@endsection