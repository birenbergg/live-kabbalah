@extends('layout.master')

@section('title')
	{{ trans('home.edit') }}: {{ $course['name_' . config('app.locale')] }}
@endsection

@section('content')
	<div class="container">
		{{ HTML::ul($errors->all()) }}

		{{ Form::model($course, array('route' => array('courses.update', $course->slug), 'method' => 'PUT')) }}
		{{ Form::open(array('url' => 'courses')) }}
			
			<div class="edit-block">
				<div>
					{{ Form::label('name_he', trans('forms.name_in_hebrew_label')) }}
					{{ Form::text('name_he', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('name_en', trans('forms.name_in_english_label')) }}
					{{ Form::text('name_en', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('description_he', trans('forms.description_in_hebrew_label')) }}
					{{ Form::textarea('description_he', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('description_en', trans('forms.description_in_english_label')) }}
					{{ Form::textarea('description_en', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('slug', trans('forms.code_name_label')) }}
					{{ Form::text('slug', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('level', trans('forms.level_label')) }}
					{{ Form::select('level', array('1' => trans('home.beginner'), '2' => trans('home.intermediate'), '3' => trans('home.advanced')), null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('price_usd', trans('forms.price_usd_label')) }}
					{{ Form:: text('price_usd', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('price_ils', trans('forms.price_ils_label')) }}
					{{ Form:: text('price_ils', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('category_id', trans('forms.category_label')) }}
					{{ Form::select('category_id', array('1' => 'קבלה למתחיל', '2' => 'היהדות והקבלה', '3' => 'לימודי רוחניות, הארה והתפתחות אישית', '4' => 'קבלה לעומק'), null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('required', trans('forms.is_required_label')) }}
					{{ Form::select('required', array('0' => trans('forms.no'), '1' => trans('forms.yes')), null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('status_en', trans('forms.status_en')) }}
					{{ Form::select('status_en', array(null => trans('forms.not_set'), '0' => trans('forms.hidden'), '1' => trans('forms.coming_soon'), '2' => trans('forms.published')), null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('status_he', trans('forms.status_he')) }}
					{{ Form::select('status_he', array(null => trans('forms.not_set'), '0' => trans('forms.hidden'), '1' => trans('forms.coming_soon'), '2' => trans('forms.published')), null) }}
				</div>
			</div>

			{{ Form::submit(trans('forms.save_button'), array('class' => 'save')) }}
		{{ Form::close() }}
	</div>
@endsection