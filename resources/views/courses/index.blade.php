@extends('layout.master')

@section('title')
    {!! trans("pages.courses_title") !!}
@stop

@section('content')
    <style type="text/css">
    .overlay {
        opacity:0.85;
        filter: alpha(opacity = 0.85);
        position:absolute;
        top:0; bottom:0; left:0; right:0;
        display:block;
        z-index:2;
        background:white;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 24px;
        font-weight: bold;
        color: #999;
    }
    </style>

    <style type="text/css">

        a {
            color: #0070FF;
            text-decoration: none;
        }

        a:hover {
            color: #832A0D;
        }

        .nav-tabs {
            text-align: center;
            font-size: 18px;
            box-sizing: content-box !important;
            margin: 2em 0;
            display: flex;
            justify-content: center;
        }

        .nav-tabs a, .nav-tabs span {
            color: black;
            padding: .5em 1em;
            display: inline-block;
        }

        .nav-tabs a {
            text-decoration: none;
            background: rgba(50, 100, 0, .1);
        }

        .nav-tabs a:hover {
            background: rgba(50, 100, 0, .2);
        }

        .nav-tabs span {
            background: rgba(50, 100, 0, .3);
        }

        .nav-tabs a:first-child,
        .nav-tabs span:first-child,
        .nav-tabs.rtl a:last-child,
        .nav-tabs.rtl span:last-child {
            border-radius: 4px 0 0 4px;
        }

        .nav-tabs a:last-child,
        .nav-tabs span:last-child,
        .nav-tabs.rtl a:first-child,
        .nav-tabs.rtl span:first-child {
            border-radius: 0 4px 4px 0;
        }

        

    </style>


    <div style="width:100%; padding: 0 0 8em;">
        <div>
            <h1 style="text-align: center; font-size: 56px; display: flex; flex-direction: column; align-items: center;">
                {{ trans('pages.courses_header') }}
            </h1>     
        </div>
        <div class="page-content">
            <div style="width: 100%; max-width: 1150px; margin: 0 auto;">
    <div style="font-size: 22px;">
        <p style="margin: .75em 0;">{{ trans('courses.preface_p1') }}</p>
        <p style="margin: .75em 0;">{{ trans('courses.preface_p2') }}</p>
        <p style="margin: .75em 0;">{{ trans('courses.preface_p3') }}</p>
        <p style="margin: .75em 0;">{{ trans('courses.preface_p4') }}</p>
    </div>
    @foreach($categories as $category)
        <h2>{{ $category['name_' . config('app.locale')] }}</h2>
        <div class="course-block">
            @foreach($category->courses as $course)
                @if ($course['status_' . config('app.locale')] !== 0 || Auth::user() && Auth::user()->role == 0)
                    @if ($course['status_' . config('app.locale')] !== 1 || Auth::user() && Auth::user()->role == 0)<a href="/courses/{{ $course->slug }}" style="color: black; text-decoration: none;">@endif
                        <div class="course-thumbnail">
                            <div class="image-wrapper" style="background-image: url({{
                                $course->img_url != '' ? $course->img_url : '//lorempixel.com/298/267/'
                            }})">
                            </div>
                            <div class="course-thumbnail-text">
                                <h4>{{ $course['name_' . config('app.locale')] }}</h4>
                                <div>
                                    <div>{{ trans('courses.level') }}: {{ $course->level['name_' . config('app.locale')] }}</div>
                                    <div>{{ trans('courses.lessons_num') }}: {{ count($course->sessions) }}</div>
                                </div>
                            </div>
                            @if ($course['status_' . config('app.locale')] == 1)
                            <div class="overlay">{{ trans('forms.coming_soon') }}</div>
                            @endif

                            @if ($course['status_' . config('app.locale')] === 0 && Auth::user() && Auth::user()->role == 0)
                            <div class="overlay">{{ trans('forms.hidden') }}</div>
                            @endif
                        </div>
                    @if ($course['status_' . config('app.locale')] !== 1 || Auth::user() && Auth::user()->role == 0)</a>@endif
                @endif
            @endforeach
        </div>
    @endforeach
</div>
        </div>
    </div>

@endsection