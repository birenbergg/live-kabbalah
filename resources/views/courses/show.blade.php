@extends('layout.master')

@section('title', $course['name_' . config('app.locale')])

@section('scripts')
    <script type="text/javascript" src="/js/audioPlayer.js"></script>
    <script type="text/javascript" src="/js/player.js"></script>
    
    <script type="text/javascript">
        audioPlayer();
    </script>
@endsection

@section('content')

    <style type="text/css">
        #playlist {
            list-style: none;
        }

        #playlist li a {
            color: black;
            text-decoration: none;
        }

        #playlist .current-audio a {
            color: blue;
        }
        audio {
            width: 100%;
            max-width: 500px;
        }
    </style>
    
    <div class="container">
        <div>
            <div id="ttt" style="display:flex; border: 1px solid #ccc; padding: 1em;margin-bottom:2em;height: 200px; background: #fff;">

                <div class="image-wrapper" style="width: 30%; height: 100%; border:1px solid #ddd; background-image: url({{
                                $course->img_url != '' ? $course->img_url : '//lorempixel.com/298/267/'
                            }})">
                </div>
                
                <div style="margin: 0 2em; flex: 1;">
                    <h1 style="margin: 0;margin-bottom:.5em;font-size: 24px;line-height: 1.2;">{{ $course['name_' . config('app.locale')] }}</h1>
                    <div id="description" class="description" style="color: #888;font-size: 18px; height: 110px; overflow: hidden;">{{ $course['description_' . config('app.locale')] }}</div>
                    <div onclick="expand()">show more...</div>
                    <div style="font-size: 18px; margin-top: 1em;">{{ trans('courses.level') }}: {{ $course->level['name_' . config('app.locale')] }}</div>
                </div>
                
                @if (Auth::user())
                    @if (!Auth::user()->courses->contains($course->id))
                        <div style="width: 30%">
                            <div>
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

                                    <input type="hidden" name="cmd" value="_xclick">
                                    <input type="hidden" name="charset" value="utf-8">
                                    <input type="hidden" name="business" value="admin@livekabbalah.org">
                                    <input type="hidden" name="item_name" value="{{ $course['name_' . config('app.locale')] }} TEST">
                                    <input type="hidden" name="item_number" value="קורס שבת">
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="hidden" name="on0" value="משתמש">
                                    <input type="hidden" name="os0" value="{{ Auth::user()->name }}">
                                    <input type="hidden" name="custom" value="1207-23">

                                    <input type="hidden" name="currency_code" value="{{ config('app.locale') == 'en' ? 'USD' : 'ILS' }}">
                                    <!-- <input type="hidden" name="amount" value="{{ config('app.locale') == 'en' ? $course->price_usd : $course->price_ils }}"> -->
                                    <input type="hidden" name="amount" value="0.01">

                                    <input type="hidden" name="for_auction" value="false">
                                    <input type="hidden" name="no_note" value="1">
                                    <input type="hidden" name="no_shipping" value="1">
                                    <input type="hidden" name="notify_url" value="http://www.universalkabbalahcommunities.org/universityhebrew/enrol/paypal/ipn.php">
                                    <input type="hidden" name="return" value="http://lku.gzmsoftware.com/courses/add/{{ $course->id }}">
                                    <input type="hidden" name="cancel_return" value="http://lku.gzmsoftware.com/courses/{{ $course->slug }}">
                                    <input type="hidden" name="rm" value="2">
                                    <input type="hidden" name="cbt" value="הקלד כאן לכניסה לקורס">

                                    <input type="hidden" name="first_name" value="{{ Auth::user()->first_name }}">
                                    <input type="hidden" name="last_name" value="{{ Auth::user()->last_name }}">
                                    <input type="hidden" name="address" value="{{ Auth::user()->address }}">
                                    <input type="hidden" name="city" value="{{ Auth::user()->city }}">
                                    <input type="hidden" name="email" value="{{ Auth::user()->email }}">
                                    <input type="hidden" name="country" value="{{ Auth::user()->country_code }}">

                                    <input type="submit" class="save" value="{{ trans('courses.take_this_course') }}">

                                </form>
                            </div>
                            <div style="text-align:center; font-weight: bold; font-size: 20px">{{ trans('courses.price') }}:
                                @if(config('app.locale') == 'en') ${{ $course->price_usd }}
                                @elseif (config('app.locale') == 'he') {{ $course->price_ils }} ₪
                                @endif
                            </div>
                        </div>
                    @endif
                @else
                    <div style="width: 30%">
                        <div>
                            <form>
                                <input type="submit" class="save" formaction="/login" value="{{ trans('courses.take_this_course') }}" />
                            </form>
                        </div>
                        <div style="text-align:center; font-weight: bold; font-size: 20px">{{ trans('courses.price') }}:
                            @if(config('app.locale') == 'en') ${{ $course->price_usd }}
                            @elseif (config('app.locale') == 'he') {{ $course->price_ils }} ₪
                            @endif
                        </div>
                    </div>
                @endif
            </div>
            
            @if (Auth::user() && Auth::user()->role == 0)
                <form style="margin-bottom:2em;">
                    <a class="save" style="display: block; text-align: center; text-decoration: none;" href="/courses/{{ $course->slug }}/edit">{{ trans('courses.edit_course') }}</a>
                </form>
            @endif
            
            <div class="lessons">

                @foreach($course->lessons as $lesson)
                    <div class="lesson">
                        <h2 style="width:auto">
                            @if (Auth::user() && Auth::user()->role == 0 || (Auth::user() && Auth::user()->courses->contains($course->id)))<a href="/courses/{{ $course->slug }}/{{ $lesson->index_number }}">@endif
                                {{ trans('courses.lesson') }} {{ $lesson->index_number }}: {{ $lesson['name_' . config('app.locale')] }}
                            @if (Auth::user() && Auth::user()->role == 0 || (Auth::user() && Auth::user()->courses->contains($course->id)))</a>@endif
                        </h2>
                        
                        <div>
                            {{ $lesson['description_' . config('app.locale')] }}
                        </div>

                        @if (Auth::user() && Auth::user()->role == 0 || (Auth::user() && Auth::user()->courses->contains($course->id)) || true)

                            <audio src="/course_resources/{{ $lesson->course->slug }}/{{ config('app.locale') }}/{{ $lesson->course_resources[0]->file_name }}.{{ $lesson->course_resources[0]->type }}" controls seekable id="audioPlayer">
                                Sorry, your browser doesn't support HTML5.
                            </audio>

                        @endif
                        
                        @if (true || Auth::user() && Auth::user()->role == 0 || (Auth::user() && Auth::user()->courses->contains($course->id)))
                        <div>
                            <p>חומרים להורדה / קישורים חיצוניים:</p>
                            <ul>
                            @foreach ($lesson->course_resources as $resource)
                                <li>
                                    @if ($resource->status)
                                        @if ($resource->type != 'link')
                                            <a href="/course_resources/{{ $lesson->course->slug }}/{{ config('app.locale') }}/{{ $resource->file_name }}.{{ $resource->type }}" download>
                                        @else
                                            <a href="{{ $resource->link }}">
                                        @endif

                                        @php
                                            $path = $_SERVER['DOCUMENT_ROOT'] . "/public/course_resources/" . $lesson->course->slug . "/" . config('app.locale') . "/" . $resource->file_name . "." . $resource->type;
                                        @endphp
                                                                        
                                        @if($resource->type == 'mp3')
                                            <i style="{{ file_exists($path) ? 'color:green' : 'color:grey' }}" class="fa fa-file-audio-o"></i>
                                        @elseif($resource->type == 'pdf')
                                            <i style="{{ file_exists($path) ? 'color:red' : 'color:grey' }}" class="fa fa-file-pdf-o"></i>
                                        @elseif($resource->type == 'link')
                                            <i style="color:skyblue" class="fa fa-globe"></i>
                                        @elseif($resource->type == 'gif' || $resource->type == 'bmp')
                                            <i style="{{ file_exists($path) ? 'color:purple' : 'color:grey' }}" class="fa fa-file-image-o"></i>
                                        @else
                                            <i style="{{ file_exists($path) ? 'color:black' : 'color:grey' }}" class="fa fa-file"></i>
                                        @endif

                                        &nbsp;

                                        {{ $resource->description }}

                                        </a>

                                    @endif
                                </li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function expand() {
            document.getElementById('ttt').style.height = 'auto';
            document.getElementById('description').style.height = 'auto';

            document.getElementById('ttt').style.maxHeight = 'auto !important';
            document.getElementById('description').style.maxHeight = 'auto !important';


            document.getElementById('description').style.overflow = 'visible';
        }
    </script>
@endsection