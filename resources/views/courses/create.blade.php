@extends('layout.master')

@section('title')
	יצירת קורס חדש
@endsection

@section('content')
	<div>
		{{ HTML::ul($errors->all()) }}

		{{ Form::open(array('url' => 'courses')) }}

			<div class="edit-block">
				<div>
					{{ Form::label('name_he', 'שם (בעברית)') }}
					{{ Form::text('name_he', Input::old('name_he')) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('name_en', 'שם (באנגלית)') }}
					{{ Form::text('name_en', Input::old('name_en')) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('description_he', 'תאור (בעברית)') }}
					{{ Form::text('description_he', Input::old('description_he')) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('description_en', 'תאור (באנגלית)') }}
					{{ Form::text('description_en', Input::old('description_en')) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('slug', 'שם קוד') }}
					{{ Form::text('slug', Input::old('slug')) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('level', 'רמה') }}
					{{ Form::select('level', array('1' => 'מתחילים', '2' => 'בינוני', '3' => 'מתקדמים'), Input::old('level')) }}
				</div>
			</div>
			
			<div class="edit-block">
				<div>
					{{ Form::label('price', 'מחיר') }}
					{{ Form:: text('price', null) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('category_id', 'קטגוריה') }}
					{{ Form::select('category_id', array('1' => 'קבלה למתחיל', '2' => 'היהדות והקבלה', '3' => 'לימודי רוחניות, הארה והתפתחות אישית', '4' => 'קבלה לעומק'), Input::old('category_id')) }}
				</div>
			</div>

			<div class="edit-block">
				<div>
					{{ Form::label('required', 'נחוץ') }}
					{{ Form::select('required', array('0' => 'לא', '1' => 'כן'), Input::old('required')) }}
				</div>
			</div>

			{{ Form::submit('צור', array('class' => 'save')) }}
		{{ Form::close() }}
	</div>
@endsection