@extends('layout.master')

@section('title')
    {!! $current_page['title_'.config('app.locale')] !!}
@stop

@section('content')

    <div style="width:100%; padding: 0 0 8em;">

        <div>
            <h1 style="text-align: center; font-size: 56px; display: flex; flex-direction: column; align-items: center;">
                {!! $current_page ? $current_page['title_'.config('app.locale')] : trans('pages.'.$current_page_header_slug.'_title') !!}
            </h1>     
        </div>

        <div class="nav-tabs {{ config('app.locale') == 'he' ? 'rtl' : '' }}">
        	@foreach($tabs as $t)
	            @if($t['available_in_'.config('app.locale')])
	        		@if($current_tab->slug != $t->slug)
	                	<a href="/{{ $current_page->slug }}/{{ $t->slug }}">
		            @else
		            	<span>
		            @endif
		            	{{ $t['tab_'.config('app.locale')] ? $t['tab_'.config('app.locale')] : $t['title_'.config('app.locale')] }}
		            @if($current_tab->slug != $t->slug)
		                </a>
		            @else
		            	</span>
		            @endif
				@endif
        	@endforeach
        </div>

        <div class="page-content">
        	@if($links)
			<div class="links-wrapper">
				@foreach($links as $l)
	            	@if($l['available_in_'.config('app.locale')])
						@if($current_link->slug != $l->slug)
							<a href="/{{ $current_page->slug }}/{{ $current_tab->slug }}/{{ $l->slug }}">
						@else
							<span>
						@endif
							{{ $l['tab_'.config('app.locale')] ? $l['tab_'.config('app.locale')] : $l['title_'.config('app.locale')] }}
						@if($current_link->slug != $l->slug)
						    </a>
						@else
							</span>
						@endif
					@endif
				@endforeach
			</div>
			@endif

			@if($sublinks)
			<div class="sublinks-wrapper">
				@foreach($sublinks as $s)
	            	@if($s['available_in_'.config('app.locale')])
	            		@if($current_sublink->slug != $s->slug)
							<a href="/{{ $current_page->slug }}/{{ $current_tab->slug }}/{{ $current_link->slug }}/{{ $s->slug }}">
						@else
						<span>
						@endif
							{{ $s['tab_'.config('app.locale')] ? $s['tab_'.config('app.locale')] : $s['title_'.config('app.locale')] }}
						@if($current_sublink->slug != $s->slug)
						    </a>
						@else
							</span>
						@endif
					@endif
				@endforeach
			</div>
			@endif

			<h2>{{ $article['title_'.config('app.locale')] }}</h2>
			{!! $article['text_'.config('app.locale')] !!}
        </div>
    </div>
@endsection