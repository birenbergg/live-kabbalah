@extends('layout.master')

@section('content')
	
	@php
		include('./php/torah.inc');
		#include('./php/holidays.inc');
		include('./php/time_gates.inc');

		# $calendar_events = file_get_contents('http://www.hebcal.com/converter/?cfg=json&gy='.date('Y').'&gm='.date('m').'&gd='.date('d').'&g2h=1');

	@endphp

	<style type="text/css">

		h3 {
			font-size: 42px !important;
			text-align: center;
		}

		h4 {
			font-size: 36px !important;
			text-align: center;
		}

		.main-street-block ul {
			list-style: none;
			margin: 0;
			padding: 0;
			display: flex;
			margin-top: 1em;
		}

		.main-street-block li {
			font-size: 18px;
			-webkit-margin-start: .5em;
			-webkit-margin-end: .5em;
		}

		.main-street-block {
			min-height: 200px;
			background-position-x: center;
			background-size: cover;

			padding: 1em 0;

			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
			/*font-weight: bold;*/
		}

		.main-street-block p {
			margin: .25em 0;
			max-width: 800px;
			padding-left: 2em;
			padding-right: 2em;
		}

		#msb-welcome {
			/*color:white;*/
			/*background-image: url(http://l7kabbalah.wpengine.com/wp-content/uploads/2015/11/slide1.jpg);*/
			/*background-image: url(https://image.shutterstock.com/z/stock-photo-magical-sunrise-with-tree-132413567.jpg);*/
			/*background-position: center;*/

		}

		#msb-portions {
			/*background-image: url(http://l7kabbalah.wpengine.com/wp-content/uploads/2015/11/home-02.jpg);*/
			/*background: black;*/
			/*background-image: url("//cdn.shopify.com/s/files/1/0199/2210/t/1/assets/bg.png?1421");*/
			/*background-image: url(https://cdn.wallpapersafari.com/15/91/8IChyc.jpg);*/
			min-height: auto;
			/*color: white;*/
		}

		#msb-month {
			background-image: url(http://l7kabbalah.wpengine.com/wp-content/uploads/2015/11/home-03.jpg);
		}

		#msb-time-gates {
			background-image: url(http://l7kabbalah.wpengine.com/wp-content/uploads/2015/11/home-51.jpg);
			color: white;
			align-items: flex-start;
		}

		#msb-upcoming-events {
			background-image: url(http://l7kabbalah.wpengine.com/wp-content/uploads/2015/12/events02.jpg);
		}

		#msb-donate {
			background-image: url(//lorempixel.com/298/267/);
		}

		#msb-social {
			background-image: url(//lorempixel.com/298/267/);
		}

		#msb-where-to-start {
			background-image: url(http://l7kabbalah.wpengine.com/wp-content/uploads/2015/11/slide3.jpg);
		}

		#msb-museum {
			/*background-image: url(/images/home/museum.jpg);*/
			/*background-size: cover;*/
			/*background-position: center;*/
			text-align: center;
		}

		p {
			font-size: 26px;
			text-align: center;
		}

		/*#msb-university {
			background-image: url(http://l7kabbalah.wpengine.com/wp-content/uploads/2016/04/bg_kabbalah_1.jpg);
		}*/

		.portion {
			background: rgba(255, 255, 255, .8);
			padding: 1em;
			display: flex;
			flex-direction: column;
			align-items: center;
			width:400px;
			border-radius: .25em;
			margin: 0 1em;
		}

		.portion h3 {
			margin-top: 0;
			text-align: center;
		}



		.portion.prev {
			background: rgba(200, 200, 150, .8);
			font-size: 60%;
			width: 350px;
		}

		.museum-button {
			color: #fff;
			border-radius: .15em;
			padding: .25em 1em;
			display: inline-block;
			margin-top: 1em;
			background-color: #0005;
			background-color: rgba(0, 0, 0, .4);
			font-weight: 300;
			-webkit-backdrop-filter: blur(5px);
		}
		.museum-button:hover {
			background-color: #0008;
			color: #fff;
		}

		a.button {
			background: dodgerblue;
			padding: 1em;
			display: flex;
			align-items: center;
			font-size: 16px;
			color: white;
			font-weight: bold;
			border-radius: .5em;
			margin: 1em;
		}

		a.button.podcast {
			/*background: white;
			color: black;
			padding: .75em;
			border: 1px solid  #999;*/
			background: #93c;
			padding: .75em;
			border: 1px solid  #999;
		}
		a.button.podcast img {
			height: 27px;
		}

		hr {
			border: 1px solid  #ddd;
			width: 800px;
		}

		.products {
			display: flex;
			margin: 50px 0;
		}

		.product {
			/*height: 250px*/
		}

		.product img {
			height: 250px;
		}

		.product p {
			width: 200px;
			font-size: 18px
		}

	</style>

	<div style="width: 100%;">
	    <div class="main-street-block" id="msb-welcome">
	    	<h3>{!! trans('home.welcome_title') !!}</h3>
			<p>{{ trans('home.welcome_p1') }}</p>
			<p>{{ trans('home.welcome_p2') }}</p>
	    </div>
	    
	    <hr />
	    
	    <div class="main-street-block" id="msb-portions">
			<h3 style="margin-top: 0;">
				{{ trans('weeks.portion') }}:
				{{ $parashoth[$current_wp[0]->wp_id - 1]['name_' . config('app.locale') ] }}
			</h3>

			<div style="display: flex; align-items: center; flex-direction: column;">
				@if($current_wp[0]['yt_video_url_' . config('app.locale')] != null)
					<iframe width="560" height="315" src="https://youtube.com/embed/{{ $current_wp[0]['yt_video_url_' . config('app.locale')] }}" frameborder="0" allowfullscreen></iframe>
				@endif
				<p style="width:560px; font-size: 18px">{{ $current_wp[0]['description_' . config('app.locale')] }}</p>
			</div>

			<div style="display: flex;">

				@if($current_wp[0]['audio_url_' . config('app.locale')] != null)
					<a class="button" href="http://j-app.me/share.aspx?lzhg={{ $current_wp[0]['audio_url_' . config('app.locale')] }}" target="_blank">
						{{ trans('home.audio_version_button') }}
					</a>
				@endif

				@if (
					str_contains($_SERVER['HTTP_USER_AGENT'], 'iPhone') === true ||
					str_contains($_SERVER['HTTP_USER_AGENT'], 'iPad') === true ||
					str_contains($_SERVER['HTTP_USER_AGENT'], 'Mac OS') === true
				)

				<a class="button podcast" href="itms://itunes.apple.com/il/podcast/%D7%9C%D7%99%D7%9E%D7%95%D7%93-%D7%96%D7%95%D7%94%D7%A8-%D7%A9%D7%91%D7%95%D7%A2%D7%99/id1102026873?mt=2">
					<i class="fa fa-podcast" style="font-size: 1.75em;"></i>
					&nbsp;
					{{ trans('home.podcast_button') }}
				</a>

				@endif
			</div>

			<div>
				<a class="button" href="/to-live-kabbalah/weekly/{{ $current_wp[0]->book_slug }}/{{ $current_wp[0]->slug }}">
					{{ trans('home.read_article') }}
					{{ $current_wp[0]->name }}
				</a>
			</div>

	    </div>

	    <hr />

	    <div class="main-street-block" id="msb-month">
	    	<h3 style="margin-top: 0;">{{ trans('home.monthly_study') }} {{ $months[$jewishMonth - 1][(isJewishLeapYear($jewishYear) ? 'leap_' : '') . 'name_' . config('app.locale')] }} – {{ trans('months.sign_of') }} {{ $months[$jewishMonth - 1]['sign_name_' . config('app.locale')] }}</h3>

			<div style="display: flex; align-items: center; flex-direction: column;">
		    	@if ($current_month_lecture[0]['yt_video_url_' . config('app.locale')] != null)
					<iframe width="560" height="315" src="https://youtube.com/embed/{{ $current_month_lecture[0]['yt_video_url_' . config('app.locale')] }}" frameborder="0" allowfullscreen></iframe>
				@else
					<img style="max-width: 560px;" src="/images/signs/{{ $months[$jewishMonth - 1][(isJewishLeapYear($jewishYear) ? 'leap_' : '') . 'slug'] }}.jpg" />
				@endif
				
				<p style="width:560px; font-size: 18px">
					{{ $current_month_lecture[0]['description_' . config('app.locale')] }}
				</p>
			</div>

			<div>
				@if ($current_month_lecture[0]['audio_url_' . config('app.locale')] != null)
					<a class="button" href="http://j-app.me/share.aspx?lzhg={{ $current_month_lecture[0]['audio_url_' . config('app.locale')] }}" target="_blank">
					{{ trans('home.listen_to_lesson') }}
					{{ $months[$jewishMonth - 1][(isJewishLeapYear($jewishYear) ? 'leap_' : '') . 'name_' . config('app.locale')] }}
				</a>				
				@endif

				<a class="button" href="/to-live-kabbalah/gates-in-time/rosh-hodesh/{{ $months[$jewishMonth - 1][(isJewishLeapYear($jewishYear) ? 'leap_' : '') . 'slug'] }}">
					{{ trans('home.read_article') }}
					{{ $months[$jewishMonth - 1][(isJewishLeapYear($jewishYear) ? 'leap_' : '') . 'name_' . config('app.locale')] }}
				</a>
			</div>

			<h4>{{ trans('home.this_month_sequences') }}</h4>

			<div style="display: flex; flex-direction: column; align-items: center; background: moccasin; border: 1px solid SaddleBrown; border-radius: .5em; padding: 0 3em 2em; background-image: url(/images/home/523766804.jpg); background-blend-mode: multiply;">
				
				<div style="font-size:120px; font-family: 'Stam'; line-height: 1.5;">
					{{ $months[$jewishMonth - 1]->planet_sign_combination }}
				</div>

				<div style="font-size:54px; font-family: 'Stam'; line-height: 1.5;">
					{{ $months[$jewishMonth - 1]->tetranomicon_combination }}
				</div>

				<div style="font-size:32px; font-family: 'Stam'; line-height: 1.5; margin: .5em 0;">
					{{ $months[$jewishMonth - 1]->ana_bekoah_line->text }}
				</div>
			
			</div>

			<div>
				@if(count($months[$jewishMonth - 1]->gates) > 0)
				<h3>מועדי החודש</h3>
				@endif

				<ul>
					@foreach($months[$jewishMonth - 1]->gates as $gate)
						<li>
							@if($gate->slug != null)
								<a href="/to-live-kabbalah/gates-in-time/holidays/{{ $gate->slug }}">
							@endif
								{{ $gate['name_' . config('app.locale')] }}
							@if($gate->slug != null)
								</a>
							@endif
						</li>
					@endforeach
				</ul>
			</div>
	    </div>

	    <hr />

	    <div class="main-street-block" id="msb-museum">
	    	<h3 style="margin:0">{{ trans('home.perfect_world_museum_title') }}</h3>
	    	<div style="margin: 0 6em;">
		    	<p>{{ trans('home.perfect_world_museum_p1') }}</p>
				<p>{{ trans('home.perfect_world_museum_p2') }}</p>
				<p>{{ trans('home.perfect_world_museum_p3') }}</p>
	    	</div>
	    	<div class="products">
	    		<div class="product">
	    			<a href="https://www.kabbalah-judaica.com/collections/main/products/challah-cover" target="_blank">
	    				<img src="/images/products/01.jpg" />
	    			</a>
	    			<p>
	    				<a href="https://www.kabbalah-judaica.com/collections/main/products/challah-cover" target="_blank">
	    					{{ trans('home.museum_product_1') }}
	    				</a>
	    			</p>
	    		</div>
	    		
	    		<!-- <div class="product">
	    			<a href="https://www.kabbalah-judaica.com/collections/main/products/brit-mila-circumcision-cushion-with-kabbalistic-names-of-protection-embroidery" target="_blank">
	    				<img src="/images/products/02.jpg" />
	    			</a>
	    			<p>
	    				<a href="https://www.kabbalah-judaica.com/collections/main/products/brit-mila-circumcision-cushion-with-kabbalistic-names-of-protection-embroidery" target="_blank">
	    					{{ trans('home.museum_product_2') }}
	    				</a>
	    			</p>
	    		</div> -->
	    		
	    		<div class="product">
	    			<a href="https://www.kabbalah-judaica.com/collections/kabbalistic-judaica/products/electroforming-silver-letters-pendant-pei-hei-lamed" target="_blank">
	    				<img src="/images/products/02.jpg" />
	    			</a>
	    			<p>
	    				<a href="https://www.kabbalah-judaica.com/collections/kabbalistic-judaica/products/electroforming-silver-letters-pendant-pei-hei-lamed" target="_blank">
	    					{{ trans('home.museum_product_2') }}
	    				</a>
	    			</p>
	    		</div>

	    		<div class="product">
	    			<a href="https://www.kabbalah-judaica.com/collections/main/products/home-protection-wall-tapestry-kabbalah-holy-names-140cm-x-101cm" target="_blank">
	    				<img src="/images/products/03.jpg" />
	    			</a>
	    			<p>
	    				<a href="https://www.kabbalah-judaica.com/collections/main/products/home-protection-wall-tapestry-kabbalah-holy-names-140cm-x-101cm" target="_blank">
	    					{{ trans('home.museum_product_3') }}
	    				</a>
	    			</p>
	    		</div>
	    		<div class="product">
	    			<a href="https://www.kabbalah-judaica.com/collections/main/products/tallit-bag-72-names" target="_blank">
	    				<img src="/images/products/04.jpg" />
	    			</a>
	    			<p>
	    				<a href="https://www.kabbalah-judaica.com/collections/main/products/tallit-bag-72-names" target="_blank">
	    					{{ trans('home.museum_product_4') }}
	    				</a>
	    			</p>
	    		</div>
	    	</div>
			<a class="button" href="https://www.kabbalah-judaica.com" target="_blank">{{ trans('home.go_to_museum_button') }}</a>
	    </div>

	    <hr />

	    <div class="main-street-block" style="flex-direction: row; align-items: flex-start;">
	    	<div style="width: 400px; text-align: end;">
	    		<img src="/images/home/donations.jpg" style="height: 300px;" />
	    	</div>
	    	<div style="max-width: 400px;">
	    		<p style="font-size: 18px !important; text-align: start">{{ trans('home.donations_p1') }}</p>
				<p style="font-size: 18px !important; text-align: start">{{ trans('home.donations_p2') }}</p>
				<p style="font-size: 18px !important; text-align: start">{{ trans('home.donations_p3') }}</p>
				<p style="font-size: 18px !important; text-align: start">{{ trans('home.donations_p4') }} <a href="/donations">{{ trans('home.donations_click') }}</a>.</p>
	    	</div>
	    </div>
	</div>
@endsection