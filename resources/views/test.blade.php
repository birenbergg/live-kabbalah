<p>Live Kabbalah is committed to helping create and/or support communities throughout the world that will provide a safe, loving environment for every human being who seeks to participate in the creation of a better world.</p>
<p>These communities are based upon the teachings of the great prophets of the Bible as well as upon the teachings of the Kabbalistic Sages that followed their vision.</p>

<h3>OUR MISSION</h3>
<p>Our mission is based on the Universal Truth that Love of the Creator is revealed when mankind displays that Love and respect to other human beings above and beyond ideology, race, religion, language or appearance. The mission of the Universal Kabbalah Communities is to empower individuals and communities to create a world of peace, love, joy, and harmony for humanity.</p>

<h3>OUR VISION</h3>
<p>Love your fellow man</p>
<ul>
	<li>Rav Ashlag, the founder of the modern renaissance of Kabbalah had a vision of a “One World Spiritual Movement”. This is the movement of “Love your fellow man as yourself”.</li>
	<li>According to Rav Ashlag’s vision this is the way to bring the world to peace through social and economic justice on a personal level and among nations.</li>
</ul>

<h3>Beehive Group Based Communities</h3>
<ul>
	<li>Rav Yehuda Ashlag teaches that the only way a person can transform, function at his best, and achieve cleaving to The Creator is when the person is in a group of people that love him, care for him and support him to become a better, more free, loving human being.</li>
	<li>Live Kabbalah is committed to help creating and/or supporting communities throughout the world that will provide a safe, loving environment for every human being who seeks to participate in the creation of a better world. These communities will be based upon the teachings of the great prophets of the Bible as well as upon the teachings of the Kabbalistic Sages that followed their vision.</li>
	<li>The transformation achieved will bring “immortality,” which can be experienced every moment as joy, happiness, success and good fortune through the bliss of The Light of the Creator.</li>
	<li>This is based on the main principle of the Abrahamic faiths that every human being is created in the image of God and his soul is a divine spark of God. Therefore, every human being has been created for and is entitled to living a life of happiness, fulfillment and bliss.</li>
</ul>

<h3>THE WISDOM</h3>
<ul>
	<li>
		<p>Traditional Teachings</p>
		<p>The study of this global movement is based on the traditional tools of Kabbalah: The Bible, The Book of Formation (Sefer Yetsira), The Zohar (Book of Splendor), The Ari’s writings, Rav Ashlag’s writings, and many traditional Kabbalists whose teachings are also based on these main bodies of work.</p>
	</li>
	<li>
		<p>Modern Teachings</p>
		<p>Live Kabbalah is open to researching and adopting some of the many modern methods and techniques that can support spiritual growth as long as those techniques align with traditional Kabbalistic teachings</p>
	</li>
</ul>

<h3>MAIN PRINCIPLES</h3>
<ul>
	<li>Every human being is a spark of The Creator, therefore, a human soul is good by nature</li>
	<li>Love your fellowman as you love yourself</li>
	<li>No coercion in spirituality</li>
	<li>Every human being should be empowered and supported to be a creator in his personal life and in the movement</li>
	<li>Rituals and beliefs are subject only to the individual’s free choice and inner faith</li>
</ul>

<h3>OUR HISTORY</h3>
<p>Kabbalah (in Hebrew) means “Wisdom that has been received”.</p>

<h4>Adam</h4>
<p>Kabbalah is the wisdom that explains the laws of the Universe, Life and God. Adam received this wisdom directly from God. Some traditions say that The Book of Raziel the Angel (Sefer Raziel HaMalakh) and others like it contain some of Adam’s wisdom.</p>

<h4>Abraham the Patriarch</h4>
<p>The wisdom was handed down through the generations from teacher to student until it reached Abraham the Patriarch. Abraham the Patriarch received revelations from The Creator that enhanced Adam’s wisdom. The Book of Formation (Sefer Yetsira) is the first great work of Kabbalistic writings in the history of Kabbalah. Abraham handed this Kabbalistic Wisdom to his son Isaac and to his grandson Jacob.</p>

<h4>Moses</h4>
<p>The line from Abraham continued until Moses, together with the whole nation of Israel, received (Kibel) the Torah from God on Mount Sinai. Moses handed down the wisdom to Joshua who handed it over to the Elders. The Judges received the wisdom from the Elders and the Prophets of the Bible received it from the Judges. The people of the Great Assembly (Anshei Knesset Gedola) received the wisdom from the Prophets and the Tannaim received it from the prophets. This lineage is described in the Mishnah in The Ethics of the Fathers.</p>

<h4>Rabbi Shimon Bar Yohai</h4>
<p>One of the Tannaim is considered by Kabbalists to be the author of the Zohar , the main work of Kabbalah. The Zohar contains the foundations of Kabbalistic faith, traditions and rituals. After writing the Zohar, the wisdom of Kabbalah and the Kabbalists went into hiding because of persecutions. From that point on the wisdom of Kabbalah was hidden and the bestowing of the Kabbalistic Ordination (Semikha) occurred “underground” until the 13th Century.</p>

<h4>The Ari (Rav Isaac Luria)</h4>
<p>The Ari was the greatest Kabbalist of the awakening of Kabbalah in the post middle ages (16th Century). The Ari left us his writings through his main student Rabbi Haim Vital. The Ari created and organized The Wisdom of Kabbalah for the modern era. He based it on all the previous ancient Kabbalistic texts, specifically The Zohar and showed how all previous teachings are parts of one inclusive highly organized cosmology. The system introduced by The Ari is called after him The Lurianic Kabbalah and is the foundation of modern theology of Sephardic Judaism and of The Hasidic Movement founded by The Baal Shem Tov (18th Century).</p>

<h4>Rav Yehuda HaLevi Ashlag</h4>
<p>Rav Ashlag was the greatest Kabbalist of the 20th Century. Rav Ashlag is responsible for the renaissance of Kabbalah in our modern time. A student and teacher of The Hasidic Movement, Rav Ashlag managed to create a new way of teaching that is a synthesis of the intellect of Lurianic Kabbalah with the heart of the Hasidic way. Rav Ashlag’s works connected the teachings of The Zohar and the writings of The Ari into one system and laid the foundation for the synthesis between Kabbalah, Modern Science and Modern Philosophy. Rav Ashlag was a great believer of Spiritual Socialism based on the humanistic principles of Kabbalah as a way to heal the miseries of humanity.</p>
<p>Live Kabbalah website has been established by students who feel the need to preserve the original teachings and to especially focus on the original vision of Rav Ashlag to combine the focus of the study of Kabbalah with the creation of spiritual communities.. These communities will be centered on building a safe, loving, supporting environment that will promote, enhance and support the journey of the individual towards cleaving to God, which is the only purpose of The Creation.</p>

<h4>Shaul Youdkevitch</h4>
<p>Shaul Youdkevitch is the person behind Live Kabbalah.</p>
<p>Shaul Youdkevitch is an internationally renowned and revered spiritual teacher. Born and raised in Israel, Shaul always had a strong interest in science. While in college working towards a degree in biology, he met a Kabbalist and began to learn about the spiritual laws of the universe. He soon realized that there was a deep connection with these teachings and the laws of science. Shaul became fascinated by the comparison and set off to learn more and more about how the universe operates both physically and spiritually.</p>
<p>After several years of studying Kabbalah, Shaul was ordained as a Rabbi and began his career as a popular and sought after teacher. Since 1984, Shaul Youdkevitch has built up Kabbalah communities in Toronto, Boca Raton, Miami, Tel Aviv, Herzlia and other locations that still exist and are thriving today. He also has created a systematic way of teaching the esoteric principles of Kabbalah, so that it is obtainable and understandable to everyone. Kabbalah teachers all around the world currently use this educational model.</p>
<p>Through the study of Ancient Kabbalistic teachings, Shaul came to understand the Universal Truth that love of the creator is revealed when mankind displays respect to other human beings above and beyond their ideology, race, religion, language or appearance. His mission is to empower individuals and communities to create a world of peace, joy and harmony for humanity. That is why he has taught and continues to teach everyone the principles of Kabbalah: the old, the young, people of all faiths and all nations.</p>
<p>Shaul Youdkevitch started the Universal Kabbalah Communities in early 2008 as a resource to teach Kabbalah and empower others to become in full alignment with themselves and their life’s purpose. Like Kabbalist Rabbi Ashlag taught in the early 20th century, he believes that the only way a person can function at his best and achieve a true and lasting connection to the Creator is to surround his or herself with loving, caring and supportive people. He is working with individuals and communities all over the world to teach this very premise through Kabbalistic principles. As the Kabbalists before him, Shaul believes that by teaching small groups of people who come together to learn spirituality and support each other, we can truly transform the world.</p>