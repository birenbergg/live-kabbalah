@extends('university.master')

@section('university-content')

<div style="width: 100%; max-width: 1150px; margin: 0 auto;">
    <div style="font-size: 22px;">
        <p style="margin: .75em 0;">{{ trans('courses.preface_p1') }}</p>
        <p style="margin: .75em 0;">{{ trans('courses.preface_p2') }}</p>
        <p style="margin: .75em 0;">{{ trans('courses.preface_p3') }}</p>
        <p style="margin: .75em 0;">{{ trans('courses.preface_p4') }}</p>
    </div>
    @foreach($categories as $category)
        <h2>{{ $category['name_' . config('app.locale')] }}</h2>
        <div class="course-block">
            @foreach($category->courses as $course)
                @if ($course['status_' . config('app.locale')] !== 0 || Auth::user() && Auth::user()->role == 0)
                    @if ($course['status_' . config('app.locale')] !== 1 || Auth::user() && Auth::user()->role == 0)<a href="/courses/{{ $course->slug }}" style="color: black; text-decoration: none;">@endif
                        <div class="course-thumbnail">
                            <div class="image-wrapper" style="background-image: url({{
                                $course->img_url != '' ? $course->img_url : '//lorempixel.com/298/267/'
                            }})">
                            </div>
                            <div class="course-thumbnail-text">
                                <h4>{{ $course['name_' . config('app.locale')] }}</h4>
                                <div>
                                    <div>{{ trans('home.level') }}: {{ $course->level['name_' . config('app.locale')] }}</div>
                                    <div>{{ trans('home.sessions_num') }}: {{ count($course->sessions) }}</div>
                                </div>
                            </div>
                            @if ($course['status_' . config('app.locale')] == 1)
                            <div class="overlay">{{ trans('forms.coming_soon') }}</div>
                            @endif

                            @if ($course['status_' . config('app.locale')] === 0 && Auth::user() && Auth::user()->role == 0)
                            <div class="overlay">{{ trans('forms.hidden') }}</div>
                            @endif
                        </div>
                    @if ($course['status_' . config('app.locale')] !== 1 || Auth::user() && Auth::user()->role == 0)</a>@endif
                @endif
            @endforeach
        </div>
    @endforeach
</div>

@stop