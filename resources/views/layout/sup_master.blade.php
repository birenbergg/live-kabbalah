<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        
        <link rel="apple-touch-icon-precomposed" href="/images/icons/iphone.png" />
        <link rel="icon" type="image/png" href="/images/icons/favicon.png" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            @if(!Route::is('home'))
                @yield('title') | 
            @endif
            {!! trans("pages.home_title") !!}
        </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Alef|Arimo|Assistant|Heebo|Raleway|Rubik|Varela+Round" rel="stylesheet">

        <!-- Styles -->
        
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
        <!-- Add the new slick-theme.css if you want the default styling -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>

        <link rel='stylesheet' media='screen and (max-width: 1365px)' href='/css/max-width-1365.css' />
        <link rel='stylesheet' media='screen and (max-width: 1024px)' href='/css/max-width-1023.css' />
        <link rel='stylesheet' media='screen and (max-width: 767px)' href='/css/max-width-767.css' />
        <link rel='stylesheet' media='screen and (max-width: 639px)' href='/css/max-width-639.css' />
        <link rel="stylesheet" type="text/css" href="/css/app.css" />
        <link rel="stylesheet" type="text/css" href="/css/common.css" />

        <!-- Font Awesone -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>


        <script id="mcjs">
            !function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/9e2d3857ba9c8cfa75da04b40/a674a716bc64b1d76a4196b94.js");
    </script>
    </head>
    <body dir="{{ config('app.locale') == 'he' ? 'rtl' : 'ltr'}}">
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div style="display: flex; justify-content: center; align-items: center; color:#5c3820; font-weight: bold;font-size: 32px; line-height: 1; direction: rtl !important;">
                        @if(!Route::is('home'))
                            <a href="{{ route('home') }}">
                        @endif

                            <span style="display:inline-block; direction: ltr; width: 200px; color:#5c3820">
                                Live<br />
                                Kabbalah
                            </span>
                        
                        @if(!Route::is('home'))
                            </a>
                        @endif

                        @if(!Route::is('home'))
                            <a href="{{ route('home') }}" style="display: flex;">
                        @endif

                            <img src="/images/logo.png" style="height:110px" />

                        @if(!Route::is('home'))
                            </a>
                        @endif

                        @if(!Route::is('home'))
                            <a href="{{ route('home') }}">
                        @endif
                            
                            <span style="display:inline-block; direction: rtl; width: 200px; color:#5c3820">
                                חיים<br />
                                קבלה
                            </span>
                            
                        @if(!Route::is('home'))
                            </a>
                        @endif
                    </div>
                    @yield('sub_master')
                </div>
            </nav>

            <div class="content">
                @yield('content')
            </div>

            <footer style="background: rgba(210, 220, 225, 1); min-height: 100px; display: flex; flex-direction: column; justify-content: center; align-items: center;margin-top: 2em; padding: 1em;">
                <div style="display: flex;">
                    <div style="max-width: 30%">
                        
                    </div>
                    <div style="max-width: 30%">
                            <a href="https://www.facebook.com/livekabbalah/"><i class="fa fa-facebook"></i></a>
                            <a href="{{ config('app.locale') == 'he' ? 'http://soundcloud.com/livekabbalah-hebrew' : 'http://soundcloud.com/livekabbalah-english' }}"><i class="fa fa-soundcloud"></i></a>
                            <a href="https://www.youtube.com/user/LiveKabbalah"><i class="fa fa-youtube"></i></a>
                        
                    </div>
                    <div style="max-width: 30%">
                        
                    </div>
                </div>
                <br>
                <a href="mailto:contact@livekabbalah.org">contact@livekabbalah.org</a>
                <br>
                Copyright &copy; 2008&ndash;@php echo date('Y'); @endphp Live Kabbalah All rights reserved.
            </footer>
        </div>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <!-- // <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script> -->
        <!-- // <script type="text/javascript" src="/js/slick-init.js"></script> -->
        <script type="text/javascript">
            @if(config('app.locale') == 'he')
                rtl = true
            @endif

            @if(config('app.locale') == 'en')
                rtl = false
            @endif
        </script>
        <!-- // <script src="//cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/1.3.7/wavesurfer.min.js"></script> -->

        <script type="text/javascript">
            $(window).scroll(function() {
                if($(window).scrollTop() > 50) {
                    $('nav').addClass('fixed');
                } else {
                    $('nav').removeClass('fixed');
                }
            });
        </script>

        @yield('scripts')
    </body>
</html>