@extends('layout.sup_master')

@section('sub_master')
    <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav">

            <!-- <li style="-webkit-margin-end: 3em;">
                    <img style="height: 3em;" src="/images/lk-logo-text-vector-{{ config('app.locale') }}.png" />
            </li> -->

            @foreach($pages as $p)
                <li>
                    @if($p['available_in_'.config('app.locale')])
                        @if(!$current_page || $current_page->slug != $p->slug)
                        <a href="/{{ $p->slug == 'home' ? '' : $p->slug }}">
                        @endif
                            {{ $p['title_'.config('app.locale')] }}
                        @if(!$current_page || $current_page->slug != $p->slug)
                        </a>
                        @endif
                    @endif
                </li>
            @endforeach

            @if(Auth::user() && Auth::user()->role == 0)
                <li>
                    @if(!Route::is('admin.dashboard'))
                        <a href="{{ route('admin.dashboard') }}">
                    @endif
                        {{ trans('menu.site_management_link') }}
                    @if(!Route::is('admin.dashboard'))
                        </a>
                    @endif
                </li>
            @endif
        </ul>

        @if(1)
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
                @if(!Route::is('login'))
                    <li><a href="{{ route('login') }}">{{ trans('menu.login_link') }}</a></li>
                @endif

                @if(!Route::is('register'))
                <li><a href="{{ route('register') }}">{{ trans('menu.register_link') }}</a></li>
                @endif
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('user.dashboard') }}">
                                {{ trans('menu.dashboard_link') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('user.logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                {{ trans('menu.logout_link') }}
                            </a>

                            <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endif
            <li>
                @if(config('app.locale') == 'en')
                    <a href="/setlocale/he">
                        עברית
                    </a>
                @endif
                @if(config('app.locale') == 'he')
                    <a href="/setlocale/en">
                        English
                    </a>
                @endif
            </li>
        </ul>
        @endif
    </div>
@stop