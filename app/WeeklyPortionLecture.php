<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeeklyPortionLecture extends Model
{
	public $timestamps = false;
}
