<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnaBekoah extends Model
{
	public $timestamps = false;
	protected $table = 'ana_bekoah';
}
