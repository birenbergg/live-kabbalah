<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeeklyPortion extends Model
{
	public $timestamps = false;
}
