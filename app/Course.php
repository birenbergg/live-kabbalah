<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	public $timestamps = false;

	protected $table = 'recorded_courses';
	
	public function category() {
		return $this->belongsTo('App\CourseCategory');
	}

	public function level() {
		return $this->belongsTo('App\Level');
	}

	public function lessons() {
		return $this->hasMany('App\Lesson');
	}

	public function users() {
		return $this->belongsToMany('App\User', 'user_to_course', 'user_id', 'course_id');
	}
}
