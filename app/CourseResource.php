<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseResource extends Model
{
	protected $table = 'course_resources';
}
