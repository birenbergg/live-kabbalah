<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gate extends Model
{
	public function month() {
		return $this->belongsToMany('App\Month', 'month_to_gate');
	}
}
