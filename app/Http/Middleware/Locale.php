<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Config;
use Cookie;
use Illuminate\Support\Facades\Session;

class Locale
{
	public function handle($request, Closure $next)
	{
		if (Session::has('applocale') AND array_key_exists(Session::get('applocale'), Config::get('languages'))) {
            App::setLocale(Session::get('applocale'));
        }
        else {
            if (Cookie::get('applocale') != null) {
                App::setLocale(Cookie::get('applocale'));
            } else {
                $cookie = Cookie::forever('applocale', Config::get('app.fallback_locale'));
                Cookie::queue($cookie);
                
                App::setLocale(Config::get('app.fallback_locale'));
            }
        }
        return $next($request);
	}
}
