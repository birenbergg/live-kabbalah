<?php

namespace App\Http\Controllers;

use Auth;
use App\Article;

// use Illuminate\Support\Facades\Validator;
// use Illuminate\Support\Facades\Input;
// use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Session;

class ArticlesController extends Controller
{    
 //    public function index() {
	// 	$categories = CourseCategory::orderby('index')->get();
	// 	return view('home', ['categories' => $categories]);
	// }

	public function show(/*$category, $subcategory, $article*/) {
		// $key = $category.'.'.$subcategory.'.'.$article;

		$article = Article::where('key', 'what_is_kabbalah.kabbalists.rashbi')->first();
		return view('what_is_kabbalah.kabbalists.rashbi', ['article' => $article]);
	}
}
