<?php

namespace App\Http\Controllers;

use Auth;
use App\Course;
use App\Lesson;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class SessionsController extends Controller
{
    public function index() {
        //
    }

    public function create()
    {
        //
    }

    public function store()
    {
        //
    }

    public function show($course, $lesson)
    {
        if (Auth::user()) {

            $course = Course::where('slug', $course)->select(array('id'))->first();

            if (Auth::user()->role == 0 || Auth::user()->courses->contains($course->id)) {
                $lesson = Lesson::where('course_id', $course->id)->where('index_number', $lesson)->with('course')->first();
                return view('lessons.show', ['lesson' => $lesson]);
            } else {
                return Redirect::to('/');
            }

        } else {
            return Redirect::to('/login');
        }
    }

    public function edit($course, $lesson)
    {
        if (Auth::user() && Auth::user()->role == 0) {

            $course = Course::where('slug', $course)->select(array('id'))->first();
            $lesson = Lesson::where('course_id', $course->id)->where('index_number', $lesson)->with('course')->first();
            
            return view('lessons.edit', ['lesson' => $lesson]);
        } else {
            return Redirect::to('/');
        }
    }

    public function update($id)
    {
        if (Auth::user() && Auth::user()->role == 0) {

            $lesson = Lesson::find($id);

            $rules = array(
                'name_he' => 'required',
                'name_en' => 'required',
                'description_he' => 'required',
                'description_en' => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('courses/' . $lesson->course->slug . '/' . $lesson->index_number . '/edit')
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            } else {

                // store
                $lesson = Lesson::find($id);

                $lesson->name_he = Input::get('name_he');
                $lesson->name_en = Input::get('name_en');

                $lesson->description_he = Input::get('description_he');
                $lesson->description_en = Input::get('description_en');

                $lesson->save();

                // redirect
                return Redirect::to('courses/' . $lesson->course->slug . '/' . $lesson->index_number);
            }
        } else {
            return Redirect::to('/');
        }
    }
}
