<?php

namespace App\Http\Controllers;

use Auth;
use App;
use App\Course;
use App\CourseCategory;
use App\Page;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CoursesController extends Controller
{    
    public function index() {
		$pages = Page::orderby('index_'.App::getLocale())->get();
		$current_page = Page::where('slug', 'courses')->first();

		$categories = CourseCategory::orderby('index')->get();
		return view('courses.index', ['pages' => $pages, 'categories' => $categories, 'current_page' => $current_page]);
	}

	public function create()
    {
    	if (Auth::user() && Auth::user()->role == 0) {
        	return view('courses.create');
        } else {
        	return Redirect::to('/');
        }
    }

    public function store()
    {
    	if (Auth::user() && Auth::user()->role == 0) {
	        $rules = array(
	            'name_he' => 'required',
	            'name_en' => 'required',
	            'description_he' => 'required',
	            'description_en' => 'required',
	            'slug' => 'required',
	            'level' => 'required',
	            'price_usd' => 'required|numeric',
	            'price_ils' => 'required|numeric',
	            'category_id' => 'required',
	            'required' => 'required'
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('courses/create')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $course = new Course;

	            $course->name_he = Input::get('name_he');
	            $course->name_en = Input::get('name_en');

	            $course->description_he = Input::get('description_he');
	            $course->description_en = Input::get('description_en');

	            $course->slug = Input::get('slug');
	            $course->level_id = Input::get('level');
	            $course->price_usd = Input::get('price_usd');
	            $course->price_ils = Input::get('price_ils');
	            $course->category_id = Input::get('category_id');
	            $course->required = Input::get('required');
	            $course->status_en = Input::get('status_en');
	            $course->status_he = Input::get('status_he');

	            $course->save();

	            // redirect
	            return Redirect::to('/');
	        }
    	} else {
        	return Redirect::to('/');
        }
    }

	public function show($slug) {
		$pages = Page::orderby('index_'.App::getLocale())->get();
		$current_page = Page::where('slug', 'courses')->first();

		$course = Course::where('slug', $slug)->with('lessons.course_resources')->first();

		// return $course;

		return view('courses.show', ['pages' => $pages, 'course' => $course, 'current_page' => $current_page]);
	}

	public function edit($slug)
	{
		if (Auth::user() && Auth::user()->role == 0) {
			$course = Course::where('slug', $slug)->first();
			return view('courses.edit', ['course' => $course]);
    	} else {
        	return Redirect::to('/');
        }
	}

	public function update($slug)
    {
    	if (Auth::user() && Auth::user()->role == 0) {

	        $rules = array(
	            'name_he' => 'required',
	            'name_en' => 'required',
	            'description_he' => 'required',
	            'description_en' => 'required',
	            'slug' => 'required',
	            'level' => 'required',
	            'price_usd' => 'required|numeric',
	            'price_ils' => 'required|numeric',
	            'category_id' => 'required',
	            'required' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('courses/' . $slug . '/edit')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $course = Course::where('slug', $slug)->first();

	            $course->name_he = Input::get('name_he');
	            $course->name_en = Input::get('name_en');

	            $course->description_he = Input::get('description_he');
	            $course->description_en = Input::get('description_en');

	            $course->slug = Input::get('slug');
	            $course->level_id = Input::get('level');
	            $course->price_usd = Input::get('price_usd');
	            $course->price_ils = Input::get('price_ils');
	            $course->category_id = Input::get('category_id');
	            $course->required = Input::get('required');
	            $course->status_en = Input::get('status_en');
	            $course->status_he = Input::get('status_he');

	            $course->save();

	            // redirect
	            return Redirect::to('/courses/' . $slug);
	        }
    	} else {
        	return Redirect::to('/');
        }
    }

    public function destroy($id)
    {
    	if (Auth::user() && Auth::user()->role == 0) {
	        // delete
	        // $post = BlogPost::find($id);
	        // $post->delete();

	        // redirect
	        return Redirect::to('/');
    	} else {
        	return Redirect::to('/');
        }
    }

    public function addCourse($course_id)
    {
        $course = Course::find($course_id);
        Auth::user()->courses()->attach($course);

        // redirect
        return Redirect::to('dashboard');
    }
}
