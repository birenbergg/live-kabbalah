<?php

namespace App\Http\Controllers;

use Auth;
use App\User;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{    
    public function index() {
		//
	}

	public function create()
    {
    	//
    }

    public function store()
    {
    	//
    }

	public function show($slug) {
		// $course = Course::where('slug', $slug)->first();
		// return view('courses.show', ['course' => $course]);
	}

	public function edit($username)
	{
		if (Auth::user() && (Auth::user()->role == 0 || Auth::user()->username == $username)) {
			$user = User::where('username', $username)->first();
			return view('user.edit', ['user' => $user]);
    	} else {
        	return Redirect::to('/');
        }
	}

	public function update($username)
    {
    	if (Auth::user() && (Auth::user()->role == 0 || Auth::user()->username == $username)) {

	        $rules = array(
	            'name' => 'required',
	            'email' => 'required|email',
	            'username' => 'required'
	        );
	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('user/' . $username . '/edit')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $user = User::where('username', $username)->first();

	            $user->name = Input::get('name');
	            $user->email = Input::get('email');
	            $user->username = Input::get('username');
	            $user->gender_id = Input::get('gender_id');
	            $user->birth_date = Input::get('birth_date');
	            $user->birth_month = Input::get('birth_month');
	            $user->birth_year = Input::get('birth_year');

	            $user->save();

	            // redirect
	            return Redirect::to('dashboard');
	        }
    	} else {
        	return Redirect::to('/');
        }
    }

    public function destroy($id)
    {
    	//
    }
}
