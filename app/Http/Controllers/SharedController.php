<?php

namespace App\Http\Controllers;

use App;
use App\Article;
use App\Page;
use App\Tab;

class SharedController extends Controller
{
	public function donations() {

		$pages = Page::orderby('index_'.App::getLocale())->get();
		$article = Article::where('slug', 'donations')->where('tab_id', null)->first();

		return view('shared_page', [
			'pages' => $pages,
			'article' => $article,
			'current_page' => null
		]);
	}

	public function about() {

		$pages = Page::orderby('index_'.App::getLocale())->get();
		$article = Article::where('slug', 'about')->where('tab_id', null)->first();

		return view('shared_page', [
			'pages' => $pages,
			'article' => $article,
			'current_page' => null
		]);
	}

	public function contact() {

		$pages = Page::orderby('index_'.App::getLocale())->get();
		$article = Article::where('slug', 'contact')->where('tab_id', null)->first();

		return view('shared_page', [
			'pages' => $pages,
			'article' => $article,
			'current_page' => null
		]);
	}
}
