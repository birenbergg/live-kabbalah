<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Course;
use App\CourseCategory;
use App\Page;
use Auth;

class UniversityController extends Controller
{
    public function index() {
		$pages = Page::orderby('index_'.App::getLocale())->get();
		$current_page = Page::where('slug', 'courses')->first();

		$categories = CourseCategory::orderby('index')->get();
		return view('courses.index', ['pages' => $pages, 'categories' => $categories, 'current_page' => $current_page]);
	}

	public function create()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
        	return view('categories.create');
        } else {
        	return view('login');
        }
    }

    public function store()
    {
    	if (Auth::user() && Auth::user()->id == 1) {
	        $rules = array(
	            'title' => 'required',
	            'slug' => 'required|unique:blog',
	            'status' => 'required|numeric',
	            'body' => 'required',
	        );

	        $validator = Validator::make(Input::all(), $rules);

	        // process the login
	        if ($validator->fails()) {
	            return Redirect::to('blog/create')
	                ->withErrors($validator)
	                ->withInput(Input::except('password'));
	        } else {
	            // store
	            $post = new BlogPost;

	            $post->title = Input::get('title');
	            $post->slug = Input::get('slug');
	            $post->date = Input::get('custom_date') ? Input::get('date') : date('Y-m-d H:i:s');
	            $post->status = Input::get('status');
	            $post->body = Input::get('body');

	            $post->save();

	            // redirect
	            Session::flash('message', 'Запись[Дюны] успешно создана!');
	            return Redirect::to('categories');
	        }
    	} else {
        	return view('login');
        }
    }

	public function show($slug) {
		$pages = Page::orderby('index_'.App::getLocale())->get();
		$current_page = Page::where('slug', 'university')->first();

		$category = CourseCategory::where('slug', $slug)->get();

		return view('categories', ['pages' => $pages, 'category' => $category[0], 'current_page' => $current_page]);
	}
}
