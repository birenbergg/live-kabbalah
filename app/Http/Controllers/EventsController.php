<?php

namespace App\Http\Controllers;

use App;
use App\Page;
use App\Event;

class EventsController extends Controller
{
	public function show() {

		$pages = Page::orderby('index_'.App::getLocale())->get();
		$current_page = Page::where('slug', 'events')->first();

		$h_events = Event::where('place', 'h')->get();
		$j_events = Event::where('place', 'j')->get();

		return view('events', [
			'pages' => $pages,
			'current_page' => $current_page,
			'h_events' => $h_events,
			'j_events' => $j_events
		]);
	}
}
