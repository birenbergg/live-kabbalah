<?php

namespace App\Http\Controllers;

use App;
use App\Article;
use App\Page;
use App\Tab;

class CommunityController extends Controller
{
	public function show($article = '') {

		$pages = Page::orderby('index_'.App::getLocale())->get();

		$current_page = Page::where('slug', 'community')->first();

		$tabs = Tab::where('page_id', $current_page->id)->orderby('index_'.App::getLocale())->get();
		if($article == '') { $article = $tabs[0]->slug; }
		$current_tab = Tab::where('slug', $article)->first();

		$article = Article::where('tab_id', $current_tab->id)->first();

		return view('page', [
			'pages' => $pages,
			'tabs' => $tabs,
			'links' => [],
			'sublinks' => [],
			'article' => $article,
			'current_page' => $current_page,
			'current_tab' => $current_tab,
			'current_sub' => $article
		]);
	}
}
