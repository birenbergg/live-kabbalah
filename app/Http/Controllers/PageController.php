<?php

namespace App\Http\Controllers;

use App;
use App\Article;
use App\Page;
use App\Tab;
use App\Link;
use App\Sublink;

class PageController extends Controller
{
	public function show($page_slug = '', $tab_slug = '', $link_slug = '', $sublink_slug = '') {

		$level = 0;

		$pages = Page::orderby('index_'.App::getLocale())->get();
		$current_page = Page::where('slug', $page_slug)->first();


		$tabs = Tab::where('page_id', $current_page->id)->orderby('index_'.App::getLocale())->get();
		if($tab_slug == '')
			$tab_slug = $tabs[0]->slug;
		else
			$level = 1;

		$current_tab = Tab::where('slug', $tab_slug)->first();



		$links = Link::where('tab_id', $current_tab->id)->orderby('index_'.App::getLocale())->get();

		if($link_slug == '')
			$link_slug = $links[0]->slug;
		else
			$level = 2;

		$current_link = Link::where('tab_id', $current_tab->id)->where('slug', $link_slug)->first();



		$sublinks = Sublink::where('link_id', $current_link->id)->orderby('index_'.App::getLocale())->get();
		if($sublink_slug == '') {
			if(count($sublinks) > 0)
				$sublink_slug = $sublinks[0]->slug;
		} else
			$level = 3;

		$current_sublink = Sublink::where('link_id', $current_link->id)->where('slug', $sublink_slug)->first();



		switch ($level) {
			case 3:
				$article = Article::where('sublink_id', $current_sublink->id)->first();
				break;

			case 2:
				$article = Article::where('link_id', $current_link->id)->first();
				break;

			case 1:
				$article = Article::where('tab_id', $current_tab->id)->first();
				break;
			
			default:
				$article = Article::where('link_id', $current_link->id)->first();
				break;
		}

		return view('page', [
			'pages' => $pages,
			'tabs' => $tabs,
			'links' => $links,
			'sublinks' => $sublinks,
			'article' => $article,
			'current_page' => $current_page,
			'current_tab' => $current_tab,
			'current_link' => $current_link,
			'current_sublink' => $current_sublink
		]);
	}
}
