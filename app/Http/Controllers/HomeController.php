<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App;
use App\Month;
use App\Page;
use App\WeeklyPortion;
use App\WeeklyPortionLecture;
use App\MonthLecture;

use Carbon\Carbon;

class HomeController extends Controller
{
    public function index() {

		$pages = Page::orderby('index_'.App::getLocale())->get();
		$current_page = Page::where('slug', 'home')->first();

		$months = Month::with('gates')->get();

		$current_month_lecture = MonthLecture::where('month_id', 6)->get();

		$parashoth = WeeklyPortion::get();
		$current_wp = WeeklyPortionLecture::where('lecture_date','<=', Carbon::now())->where('shabbath_date','>=', Carbon::now())->get();

		return view('home', [
			'months' => $months,
			'parashoth' => $parashoth,
			'current_month_lecture' => $current_month_lecture,
			'current_wp' => $current_wp,
			'pages' => $pages,
			'current_page' => $current_page
		]);
	}
}
