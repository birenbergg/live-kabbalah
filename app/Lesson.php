<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
	public $timestamps = false;
	
	public function course() {
		return $this->belongsTo('App\Course');
	}
	
	public function course_resources() {
		return $this->hasMany('App\CourseResource');
	}
}
