<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
	public $timestamps = false;
	
	public function ana_bekoah_line() {
		return $this->belongsTo('App\AnaBekoah');
	}

	public function gates() {
		return $this->belongsToMany('App\Gate', 'month_to_gate');
	}

}
