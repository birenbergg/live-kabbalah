function audioPlayer() {
    var currentAudio = 0;
    $('#audioPlayer')[0].src = $('#playlist li a')[0];

    $('#playlist li a, a.play-lesson').click(function(e) {
        e.preventDefault();
        $('#audioPlayer')[0].src = this;
        $('#audioPlayer')[0].play();

        $('a.play-lesson i').removeClass('fa-pause');
        $('a.play-lesson i').addClass('fa-volume-up');
        currentAudio = $(this).parent().index();
        
        $(this).find('i').removeClass("fa-volume-up");
        $(this).find('i').addClass("fa-pause");
    });

    $('#audioPlayer')[0].addEventListener('ended', function() {
        currentAudio++;

        if (currentAudio == $('playlist li a').length) {
            currentAudio == 0;
        }
        $('a.play-lesson i').removeClass('fa-pause');
        $('a.play-lesson i').addClass('fa-volume-up');

        $('a.play-lesson i:eq('+currentAudio+')').removeClass('fa-volume-up');

        $('a.play-lesson i:eq('+currentAudio+')').addClass('fa-pause');

        $('#audioPlayer')[0].src = $('#playlist li a')[currentAudio].href;
        $('#audioPlayer')[0].play();
    });
}