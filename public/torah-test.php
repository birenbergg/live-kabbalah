<html>
<body>

<form name="torahform" action="torah-test.php">
Day: <input type="text" name="day" size="2">
Month: <input type="text" name="month" size="2">
Year: <input type="text" name="year" size="4">
<input type="checkbox" name="diaspora" value="X">Diaspora
<input type="submit" value="Show name of torah section(s)">
</form>

<?php
include('./php/torah.inc');

if (isSet($_REQUEST["day"]) && isSet($_REQUEST["month"]) && isSet($_REQUEST["year"])) {
  $paramDay = intval($_REQUEST["day"]);
  $paramMonth = intval($_REQUEST["month"]);
  $paramYear = intval($_REQUEST["year"]);
  if ($_REQUEST["diaspora"] == "X") {
    $isDiaspora = true;
  } else {
    $isDiaspora = false;
  }
  echo "<p>$paramDay.$paramMonth.$paramYear, diaspora = $isDiaspora</p>\n";
  $str = getTorahSections(9, 9, 2017, false);
  if ($str != "") {
    echo "<p>Torah section(s): $str</p>\n";
  } else {
    echo "<p>No torah sections on that day</p>\n";
  }
}
?>

</body>
</html>