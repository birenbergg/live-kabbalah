<?php

function isJewishLeapYear($year) {
	return ($year % 19 == 0 || $year % 19 == 3 || $year % 19 == 6 || $year % 19 == 8 || $year % 19 == 11 || $year % 19 == 14 || $year % 19 == 17);
}

$jdNumber = gregoriantojd(date('n'), date('j'), date('Y'));

#$jdNumber = gregoriantojd(6, 28, 2017);

$jewishDate = jdtojewish($jdNumber);

list($jewishMonth, $jewishDay, $jewishYear) = explode('/', $jewishDate);


function toHebNumbers($num, $heb) {
	if($heb) {
		$alphabet = ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ז', 'ח', 'ט', 'י', 'כ', 'ל', 'מ', 'נ', 'ס', 'ע', 'פ', 'צ', 'ק', 'ר', 'ש', 'ת'];

		$unit 		= ($num / pow(10, 0)) % 10;
		$ten 		= ($num / pow(10, 1)) % 10;
		$hundred 	= ($num / pow(10, 2)) % 10;

		$heb_unit;
		$heb_ten;
		$heb_hundred;

		if($ten == 1 && ($unit == 5 || $unit == 6)) {
			$heb_unit 	= $unit == 0 ? '' : $alphabet[$unit];
			$heb_ten 	= $ten == 0 ? '' : $alphabet[8];
		} else {
			$heb_unit 	= $unit == 0 ? '' : $alphabet[$unit - 1];
			$heb_ten 	= $ten == 0 ? '' : $alphabet[$ten + 8];
		}

		if ($hundred < 5) {
			$heb_hundred = $hundred == 0 ? '' : $alphabet[$hundred + 17];
		} else if ($hundred < 9) {
			$heb_hundred = $hundred == 0 ? '' : $alphabet[21] . $alphabet[$hundred + 13];
		} else {
			$heb_hundred = $hundred == 0 ? '' : $alphabet[21] . $alphabet[21] . $alphabet[18];
		}

		$result_array = [];

		if($heb_hundred != '')	array_push($result_array, $heb_hundred);
		if($heb_ten != '')		array_push($result_array, $heb_ten);
		if($heb_unit != '')		array_push($result_array, $heb_unit);

		$result_string = '';

		for ($i = 0; $i < count($result_array); $i++) {
			if (count($result_array) > 1 && $i == count($result_array) - 1) {
				$result_string .= '״';
			}
			$result_string .= $result_array[$i];
		}

		return $result_string;
	} else {
		return $num;
	}
}